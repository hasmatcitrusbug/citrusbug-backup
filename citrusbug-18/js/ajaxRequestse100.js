;(function ($) {

    $('.blog-search .cat-item').click(function (e) {

        // if we click on item again
        if ($(this).hasClass('current-cat')) {
            return false;
        }

        showSpinner(true);
        emptyAjaxLoadMoreContainer();

        $('.blog-search .cat-item').removeClass('current-cat');
        $(this).addClass('current-cat');

        var classList = $(this).attr('class').split(/\s+/);
        var catId = 0;

        $.each(classList, function(index, item) {
            var match = item.match(/cat-item-(\d+)/i);

            if (!match) {
                return;
            }

            catId = item.match(/cat-item-(\d+)/i)[1];
        });

        var href = $('a', this).attr('href').replace(/http(s)?:\/\//, '//');
        // using html5 history api
        window.history.replaceState('replaceUrl', 'blogCategory', href);

        href = href.split('../index.html'); //.substr(href.lastIndexOf('/'));
        var catSlug = href = href[href.length - 1] === '' ? href[href.length - 2] : href[href.length - 1];

        if (!catId) {
            // empty slug used for loading all items
            catSlug = '';
        }

        loadByAttrs(catSlug, '');

        return false;
    });

    function getItemsByCategory(catId, callback) {
        // ajax_object generate by wordpress
        var url = ajax_object.ajax_url;
        var data = {
            categoryId: catId,
            action: 'posts_by_category'
        };

        $.ajax({
            url: url,
            data: data,
            method: 'post',
            dataType: 'json',
            success: function(response) {
                callback(null, response);
            },
            error: function (xhr) {
                callback(true, xhr);
            }
        });
    }

    function emptyAjaxLoadMoreContainer() {
        $('#ajax-load-more .alm-ajax').empty();
        $('#ajax-load-more .alm-btn-wrap').remove();
    }

    function appendHtml(html) {
        $('.blog-list-wrap').html(html);
        $("#ajax-load-more").ajaxloadmore();
    }

    function showSpinner(show) {
        return show ? $('.alm-btn-wrap button').addClass('loading') : $('.alm-btn-wrap button').removeClass('loading');
    }

    // ajax load more plugin callback
    $.fn.almComplete = function(alm) {
        bindEvents();
    };

    function getItemsByTag(tagId, callback) {
        var url = ajax_object.ajax_url;
        var data = {
            tagId: tagId,
            action: 'posts_by_tag'
        };

        $.ajax({
            url: url,
            data: data,
            method: 'post',
            dataType: 'json',
            success: function(response) {
                callback(null, response);
            },
            error: function (xhr) {
                callback(true, xhr);
            }
        });
    }

    function loadByTag(elem) {
        $('.blog-search .cat-item').removeClass('current-cat');

        showSpinner(true);
        emptyAjaxLoadMoreContainer();

        var href = $(elem).attr('data-link').replace(/http(s)?:\/\//, '//');

        window.history.replaceState('replaceUrl', 'blogTag', href);

        var tagId = $(elem).attr('data-id');
        href = href.split('../index.html');
        var tagSlug = href[href.length - 1] === '' ? href[href.length - 2] : href[href.length - 1];

        loadByAttrs('', tagSlug);
    }

    function loadByAttrs(cat, tag) {
        $('#ajax-load-more').attr('data-tag', tag);
        $('#ajax-load-more .alm-ajax').attr('data-tag', tag);

        $('#ajax-load-more').attr('data-category', cat);
        $('#ajax-load-more .alm-ajax').attr('data-category', cat);

        $("#ajax-load-more").ajaxloadmore();
    }

    function bindEvents() {

        $('.blog-list-wrap .blog-item .tag-wrap .tag-item').off( 'click' ).on('click', function (e) {
            loadByTag(this);
        });

        $('.top-stories-wrap .tag-wrap .tag-item').off( 'click' ).on('click', function (e) {
            var scrollTop = $("#blog-list-holder").offset().top - parseInt($("#blog-list-holder").css('marginTop'));

            $('html, body').animate({
                scrollTop: scrollTop
            }, 1000);

            loadByTag(this);
        });
    }

})(jQuery);
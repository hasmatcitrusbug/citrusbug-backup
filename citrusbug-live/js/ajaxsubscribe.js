jQuery(document).ready(function() {
    jQuery("#submit").submit(function() {
		//alert('here');return false;
        var name = jQuery("#name").val();
		var email = jQuery("#email").val();
		var message = jQuery("#message").val();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var baseurl = jQuery('#baseurl').val();
        $("#ninja_forms_field_6_error").hide();
        $("#ninja_forms_field_7_error").hide();
        
        
        if (name == '') {
            $("#ninja_forms_field_6_error").show();
            return false;
        }else if (email == '') {
            $("#ninja_forms_field_7_error").show();
            return false;
        } else if (email != '' && regex.test(email) == !1) {
            $("#ninja_forms_field_7_error").show();
            return false;
        }else {
			
            var postdata = {};
            postdata.name = name;
            postdata.email = email;            
			postdata.message = message;
			
            jQuery.post('ajaxsubscribeform.php', postdata, function(result) {
				
				var response = jQuery.parseJSON(result);
				//console.log(response);return false;
				if(response.message== 'success')
				{
					
					jQuery("#mail-success").css('display', 'block');
                    $('input[type="text"],textarea').val('');
                    $('.unsubscribe_form').hide();
					
				}else{
					jQuery("#mail-fail").css('display', 'block');
				} 
				
			});
			
			return false;
		
	
        }
         
        
    });
   
   
});